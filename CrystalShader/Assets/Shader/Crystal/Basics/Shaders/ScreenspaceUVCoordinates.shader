Shader "Basics/ScreenspaceUVCoordinates"
{
    Properties
    {
        _MainTex("Main Texture", 2D) = "white" {}
        //when test with color
        //_Color("Color",Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags{ "RenderType" = "Opaque" "Queue" = "Transparent"}
        Pass
        {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            //sampler2D _MainTex;
            //float4 _MainTex_ST;
            float4 _Color;
            
            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD0;//screen position
            };

            v2f vert(appdata v)
            {
                v2f o;
                //convert the vertex positions from object space to clip space so they can be rendered
                o.vertex = UnityObjectToClipPos(v.vertex);
                float4 tempScreenPos = ComputeScreenPos(o.vertex);//screen position normals

                // sample the texture
                float2 textureCoordinate = tempScreenPos.xy / tempScreenPos.w;
                
                //roba che centra nello schermo
                //float aspect = _ScreenParams.x / _ScreenParams.y;//center the motherfucker
                //textureCoordinate.x = textureCoordinate.x * aspect;
                //textureCoordinate = TRANSFORM_TEX(textureCoordinate, _MainTex);

                o.screenPos = float4(textureCoordinate, 0, 1);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                
                //screen position normals "with color and _MainTex"
                /*
                REMEMBER THAT YOU NEED TO ACTIVATE THE PROPERTIES
                */
                //fixed4 col = tex2D(_MainTex, i.screenPos);
                //col *= _Color;
                //return col;
                
                //screen position normals "basic as fuck"
                return i.screenPos;
            }
            ENDCG
        }
    }
}