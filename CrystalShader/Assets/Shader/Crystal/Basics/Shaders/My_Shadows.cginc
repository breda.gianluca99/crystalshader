#if !defined(MY_SHADOWS_INCLUDED)
#define MY_SHADOWS_INCLUDED

#include "UnityCG.cginc"

struct VertexData {
	float4 position : POSITION;
	float3 normalbias : NORMAL;
};

float4 MyShadowVertexProgram (VertexData v) : SV_POSITION {
	//UnityClipSpaceShadowCasterPos converte a world space applica il normal bias e poi lo converte in clip space
	float4 position = UnityClipSpaceShadowCasterPos(v.position.xyz,v.normalbias);
	return UnityApplyLinearShadowBias(position);
}

half4 MyShadowFragmentProgram () : SV_TARGET {
	return 0;
}

#endif