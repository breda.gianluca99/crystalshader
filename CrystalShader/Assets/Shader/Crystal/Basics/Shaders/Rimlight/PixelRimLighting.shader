Shader "Basics/PixelRimLight"
{
    Properties
    {
        _MainTex("Main Texture", 2D) = "white" {}
        _FresnelPower("Fresnel Power", Float) = 1
        _FresnelWeight("Fresnel Weight", Float) = 1
        _FresnelCol("Fresnel Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _FresnelPower;
            float _FresnelWeight;
            float4 _FresnelCol;

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normals : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 screenPos : TEXCOORD1;
                float3 normal : TEXCOORD2;
                float3 worldPos : TEXCOORD3;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);//get world position of the object in worldspace through moltiplication of the matrix3x3 for object to world
                o.vertex = UnityObjectToClipPos(v.vertex);//from object space to Clip space for the GPU to render
                o.screenPos =  normalize(UnityObjectToViewPos(v.normals)); //Transforms a point from object space to view space.
                o.normal = v.normals;
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture

                //view direction from camera: 
                //normalize the multiplication beteween the object in world space and clip space (float4 vertex : POSITION, float4 vertex : SV_POSITION) 
                //and after this subtract the World space to camera coordinate to the now calculated object in world position
                //to get the view direction of the camera
                float3 viewDir = normalize(i.worldPos - _WorldSpaceCameraPos.xyz);

                //get the world normals from the object space of the object
                float3 worldNormal = UnityObjectToWorldNormal(i.normal);
                
                //Value is 0 if looking at surface; 1 if view direction is perpendicular to the surface
                float rimT = 1 + dot(viewDir,worldNormal);

                //Increasing the power makes the effect stronger at shallower viwwing angles
                float fresnel = pow(rimT, _FresnelPower);

                //Control the overall strenght and colour of the effect
                float3 rimCol = fresnel * _FresnelWeight * _FresnelCol;

                float4 tex = tex2D(_MainTex,i.uv);

                return tex + float4(rimCol,1);
            }
            ENDCG
        }
    }
}