Shader "Custom/CrystalShineGrabPassMult"
{
    Properties
    {
        _MainTex("Main Texture", 2D) = "white" {}
        _ShineTex("Shine Texture", 2D) = "white" {}
        _AlphaTex("Alpha Texture", 2D) = "white" {}
        _Color("Crystal Color", Color) = (1,1,1,1)
        _ShineStrength("Shine Strength", Range(0,1)) = 0.4
        _Distorsion("Distorsion Strength", Range(0,1)) = 0.1
        
        //_FresnelPower("Fresnel Power", Float) = 1
        //_FresnelWeight("Fresnel Weight", Float) = 1
        //_FresnelCol("Fresnel Color", Color) = (1,1,1,1)
        
        //_Transparency("Transparency", Float) = 1
    }
    SubShader
    {
        Tags{ "RenderType" = "Transparent" "Queue" = "Transparent"}
        // Grab the texture behind the object
        GrabPass
        {}

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            sampler2D _ShineTex;
            float4 _ShineTex_ST;

            sampler2D _AlphaTex;
            float4 _AlphaTex_ST;
            
            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _GrabTexture;
            float4 _Color;
            float _ShineStrength;
            float _Distorsion;

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normals : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD0;//screen position
                float3 normal : TEXCOORD1;
                float2 uv : TEXCOORD2;
                float4 grabPos : TEXCOORD3;
                //float3 viewDir : TEXCOORD4;
                //float3 worldNormal : TEXCOORD5;
            };

            v2f vert(appdata v)
            {
                v2f o;
                //convert the vertex positions from object space to clip space so they can be rendered
                o.vertex = UnityObjectToClipPos(v.vertex);
                float4 tempScreenPos = ComputeScreenPos(o.vertex);//screen position normals

                // sample the texture
                float2 textureCoordinate = tempScreenPos.xy / tempScreenPos.w;
                
                //Transforms normal from object to world space
                //o.worldNormal = UnityObjectToWorldNormal(v.normals);
                
                o.screenPos = float4(textureCoordinate, 0, 1);

                o.normal =  normalize(UnityObjectToViewPos(v.normals)); //Transforms a point from object space to view space.
                o.grabPos = ComputeGrabScreenPos(o.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                //float4 alph = tex2D(_AlphaTex,i.uv);
                float4 mainTex = tex2D(_MainTex, i.uv);

                float4 alphaTex = tex2D(_AlphaTex, i.uv);
                
                float4 c;
                c.rgb = mainTex.rgb;
                c.a = alphaTex.a;
                c *= _Color;
                
                float shineUVs = i.screenPos.x + i.screenPos.y + i.normal.x + i.normal.y + c.a;
                float4 shine = tex2D(_ShineTex,float2(shineUVs,0));

                float2 refractionsUV = i.screenPos.xy + i.normal.xy * _Distorsion;
                
                float4 refr = tex2D(_GrabTexture, refractionsUV) + c;

                float4 base = shine * (c + _ShineStrength);

                return base + refr;
                //return float4(rimCol,1);
            }
            ENDCG
        }
    }
}