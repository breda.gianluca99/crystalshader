Shader "Custom/CrystalShineGrabPassMultRimPixel"
{
    Properties
    {
        _MainTex("Main Texture", 2D) = "white" {}
        _ShineTex("Shine Texture", 2D) = "white" {}
        _AlphaTex("Alpha Texture", 2D) = "white" {}
        _Color("Crystal Color", Color) = (1,1,1,1)
        _ShineStrength("Shine Strength", Range(0,1)) = 0.4
        _Distorsion("Distorsion Strength", Range(0,1)) = 0.1

        _FresnelPower("Fresnel Power", Float) = 2
        _FresnelWeight("Fresnel Weight", Float) = 1
        _FresnelCol("Fresnel Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags{ "RenderType" = "Transparent" "Queue" = "Transparent"}
        // Grab the texture behind the object
        GrabPass
        {}

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            sampler2D _ShineTex;
            float4 _ShineTex_ST;

            sampler2D _AlphaTex;
            float4 _AlphaTex_ST;
            
            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _GrabTexture;
            float4 _Color;
            float _ShineStrength;
            float _Distorsion;

            float _FresnelPower;
            float _FresnelWeight;
            float4 _FresnelCol;

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normals : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD0;//screen position
                float3 normal : TEXCOORD1;
                float3 screenNorm : TEXCOORD2;
                float2 uv : TEXCOORD3;
                float3 viewDir : TEXCOORD5;
                float3 worldNormal : TEXCOORD6;
                float3 worldPos : TEXCOORD7;
                //float4 shineScreenPos : TEXCOORD8;//shine screen position
            };

            v2f vert(appdata v)
            {
                v2f o;
                //convert the vertex positions from object space to clip space so they can be rendered
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                
                float4 tempScreenPos = ComputeScreenPos(o.vertex);//screen position normals
                float2 textureCoordinate = tempScreenPos.xy / tempScreenPos.w;
                
                o.screenPos = float4(textureCoordinate, 0, 1);

                o.worldPos = mul(unity_ObjectToWorld, v.vertex);//get world position of the object in worldspace through moltiplication of the matrix3x3 for object to world
                o.viewDir = normalize(o.worldPos - _WorldSpaceCameraPos.xyz);//to get the view direction of the camera

                o.normal = v.normals;
                o.worldNormal = UnityObjectToWorldNormal(o.normal);//get the world normals from the object space of the object
                o.screenNorm =  normalize(UnityObjectToViewPos(v.normals)); //Transforms a point from object space to view space.

                return o;
            }
            fixed4 frag(v2f i) : SV_Target
            {
                //float4 alph = tex2D(_AlphaTex,i.uv);
                float4 mainTex = tex2D(_MainTex, i.uv);
                float4 alphaTex = tex2D(_AlphaTex, i.uv);
                float4 c;
                c.rgb = mainTex.rgb;
                c.a = alphaTex.a;
                c *= _Color;
                
                float shineUVs = i.screenPos.x + i.screenPos.y + i.screenNorm.x + i.screenNorm.y + c.a;
                float4 shine = tex2D(_ShineTex,float2(shineUVs,0));

                float2 refractionsUV = i.screenPos.xy + i.screenNorm.xy * _Distorsion;
                float4 refr = tex2D(_GrabTexture, refractionsUV) + c;
                
                float4 base = shine * (c + _ShineStrength);

                //view direction from camera: 
                //normalize the multiplication beteween the object in world space and clip space (float4 vertex : POSITION, float4 vertex : SV_POSITION) 
                //and after this subtract the World space to camera coordinate to the now calculated object in world position
                
                //Value is 0 if looking at surface; 1 if view direction is perpendicular to the surface
                float rimT = 1 + dot(i.viewDir,i.worldNormal);
                //Increasing the power makes the effect stronger at shallower viwwing angles
                float fresnel = pow(rimT, _FresnelPower);
                //Control the overall strenght and colour of the effect
                float3 rimCol = fresnel * _FresnelWeight * _FresnelCol;
               
                return base + float4(rimCol,1) + refr;
                //return float4(rimCol,1);
            }
            ENDCG
        }
    }
}