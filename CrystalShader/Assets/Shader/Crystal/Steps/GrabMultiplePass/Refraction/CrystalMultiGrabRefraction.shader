Shader "Custom/CrystalMultiGrabRefraction"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags{ "RenderType" = "Opaque" "Queue" = "Transparent"}

        // Grab the texture behind the object
        GrabPass{}

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            sampler2D _GrabTexture;
            float4 _Color;

            struct appdata
            {
                float4 vertex : POSITION;
                float4 grabPos : TEXCOORD0;
                float3 normals : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD0;//screen position
                float3 normal : TEXCOORD1;
                float4 grabPos : TEXCOORD2;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);

                float4 tempScreenPos = ComputeScreenPos(o.vertex);//screen position normals
                // sample the texture
                float2 textureCoordinate = tempScreenPos.xy / tempScreenPos.w;
                //float aspect = _ScreenParams.x / _ScreenParams.y;//center the motherfucker
                //textureCoordinate.x = textureCoordinate.x * aspect;
                //textureCoordinate = TRANSFORM_TEX(textureCoordinate, _MainTex);
                o.screenPos = float4(textureCoordinate, 0, 1);

                // use UnityObjectToClipPos from UnityCG.cginc to calculate 
                // the clip-space of the vertex
                // use ComputeGrabScreenPos function from UnityCG.cginc
                // to get the correct texture coordinate
                o.normal =  normalize(UnityObjectToViewPos(v.normals)); //local space to view space
                
                o.grabPos = ComputeGrabScreenPos(o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 refractionsUV = i.screenPos.xy + i.normal.xy * 0.1;
                float4 col = tex2D(_GrabTexture, refractionsUV) + _Color;
                return col;
                //return bgcolor * _Color * ruv;
            }
            ENDCG
        }
    }
}
