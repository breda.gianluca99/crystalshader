Shader "Custom/CrystalShine"
{
    Properties
    {
		_MainTex("Main Texture", 2D) = "white" {}
        _ShineTex("Shine Texture", 2D) = "white" {}
        _AlphaTex("Alpha Texture", 2D) = "white" {}
        _Color("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags{ "RenderType" = "Transparent" "Queue" = "Transparent"}
        Pass
        {
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            sampler2D _ShineTex;
            float4 _ShineTex_ST;

            //sampler2D _AlphaTex;
            //float4 _AlphaTex_ST;
            
            float4 _Color;
            
            struct appdata
            {
                float4 vertex : POSITION;
                float3 normals : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD0;//screen position
                float3 normal : TEXCOORD1;
            };

            v2f vert(appdata v)
            {
                v2f o;
                //convert the vertex positions from object space to clip space so they can be rendered
                o.vertex = UnityObjectToClipPos(v.vertex);
                float4 tempScreenPos = ComputeScreenPos(o.vertex);//screen position normals

                // sample the texture
                float2 textureCoordinate = tempScreenPos.xy / tempScreenPos.w;
                //float aspect = _ScreenParams.x / _ScreenParams.y;//center the motherfucker
                //textureCoordinate.x = textureCoordinate.x * aspect;
                //textureCoordinate = TRANSFORM_TEX(textureCoordinate, _ShineTex);
                o.screenPos = float4(textureCoordinate, 0, 1);
                o.normal =  normalize(UnityObjectToViewPos(v.normals)); //local space to view space
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                //float4 alph = tex2D(_AlphaTex,i.uv);
                
                float x = i.screenPos.x + i.screenPos.y + i.normal.x + i.normal.y;
                
                
                //it should be this, but becaouse idk how ot sample a texture over another one with alpha clip with c.a 
                //float x = i.screenPos.x + i.screenPos.y + i.normal.x + i.normal.y + alph.a;
                
                float4 shine = tex2D(_ShineTex,float2(x,0));

                return shine;
            }
            ENDCG
        }
    }
}